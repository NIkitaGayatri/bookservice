package org.com.restcontrollers;

import java.util.List;

import org.com.entities.Book;
import org.com.exception.BookNotFoundException;
import org.com.services.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/")
@Api(description = "This is simple bookService we can use this api to add,read,update and delete books" )
@ApiResponses(value = { @ApiResponse(code = 200, message = "Successfully added new book"),
		@ApiResponse(code = 401, message = "Unauthorized", response = BookNotFoundException.class),
		@ApiResponse(code = 403, message = "Forbidden", response = BookNotFoundException.class),
		@ApiResponse(code = 404, message = "The resource you are trying is not found", response = BookNotFoundException.class),
		@ApiResponse(code = 500, message = "Internal Sever error", response = BookNotFoundException.class) })
public class BookRestController {
	@Autowired
	BookService bookSevice;

	@GetMapping("books")
	@ApiOperation(value = "Displaying all books", response = Iterable.class)
	public ResponseEntity<List<Book>> getAllBooks() {
		return new ResponseEntity<List<Book>>(bookSevice.getAllBooks(), HttpStatus.OK);
	}

	@GetMapping("book/{id}")
	@ApiOperation(value = "Getting a Specific book", response = Book.class)
	public ResponseEntity<Book> getBookById(@PathVariable int id) {
		return new ResponseEntity<Book>(bookSevice.getBookById(id), HttpStatus.OK);

	}

	@PostMapping(value = "/book")
	@ApiOperation(value = "Adding a new book", response = Book.class)
	public ResponseEntity<Book> addBook(
			@ApiParam(value = "Book model that needs to be added ", required = true) @RequestBody Book book) {
		return new ResponseEntity<Book>(bookSevice.createBook(book),HttpStatus.CREATED);
	}

	@DeleteMapping("book/{id}")
	@ApiOperation(value = "Deleting a book", response = Book.class)
	public ResponseEntity<?> deleteBook(@PathVariable int id) {
		bookSevice.deleteBook(id);
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}

	@PutMapping("book/{id}")
	@ApiOperation(value = "Updating a book", response = Book.class)
	public ResponseEntity<Book> updateBook(@PathVariable int id, @RequestBody Book book) {
		bookSevice.updateBook(id, book);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
}
