package org.com.exception;

import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionController {
	private final static Logger logger=LogManager.getLogger("ExceptionController.class");
	@ExceptionHandler
	public ResponseEntity<Object> exceptionHandler(final HttpServletRequest request,BookNotFoundException exception) {
		logger.error(request.getRequestURI());
		ExceptionResponse exceptionResponse=new ExceptionResponse(exception.getMessage(),request.getRequestURI());
		return new ResponseEntity<>(exceptionResponse,HttpStatus.NOT_FOUND);
	}
}
