package org.com.exception;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
/**
 * This class is used to execute when an 
 * book is not found raising an exception 
 * book not found
 * 
 */
@SuppressWarnings("serial")
public class BookNotFoundException extends RuntimeException {
	private static final Logger log = LogManager.getLogger(BookNotFoundException.class);

	public BookNotFoundException(String message) {
		super(message);
		log.info(message);
	}
}
