package org.com.exception;

/**
 * This class is executes to send the exception response to RestFullAPI when an
 * exception arises
 */

public class ExceptionResponse {

	private String errorMessage;
	private String requestedURI;

	public ExceptionResponse() {

	}

	public ExceptionResponse(String errorMessage, String requestedURI) {
		super();
		this.errorMessage = errorMessage;
		this.requestedURI = requestedURI;
	}

	/**
	 * This method is used to return error message.
	 * 
	 * @return String This returns error message.
	 */
	public String getErrorMessage() {
		return errorMessage;
	}

	/**
	 * This method is used to set error message.
	 * 
	 * @param errorMessage is used to set errorMessage to current object
	 */
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	/**
	 * This method is used to return URI.
	 * 
	 * @return String This returns URI of error path.
	 */
	public String getRequestedURI() {
		return requestedURI;
	}

	/**
	 * This method is used to set URI path.
	 * 
	 * @param requestedURI is used to set to current object
	 * 
	 */

	public void setRequestedURI(String requestedURI) {
		this.requestedURI = requestedURI;
	}

	/**
	 * This method is used to return error message.
	 * 
	 * @return String returns error message.
	 */
	@Override
	public String toString() {
		return errorMessage;
	}

}
