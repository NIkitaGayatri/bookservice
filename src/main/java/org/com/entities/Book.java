package org.com.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Book {
	
	@Id
	@ApiModelProperty(notes="Specific id corresponding to the book")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@ApiModelProperty(notes="The book name of a book")
	private String bookName;
	@ApiModelProperty(notes="The author of a book")
	private String author;
	@ApiModelProperty(notes="Type of a book")
	private String genre;
	@ApiModelProperty(notes="Desription about book")
	private String description;
}
