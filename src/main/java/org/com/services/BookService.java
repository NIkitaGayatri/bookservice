package org.com.services;

import java.util.List;

import org.com.entities.Book;
import org.com.exception.BookNotFoundException;
import org.com.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
/**
 * This class is used to perform all BookService Operations like getting all
 * books,finding Book By Id,adding Book,deleting Book, Updating Book
 * 
 */
public class BookService {
	@Autowired
	BookRepository bookRepository;

	/**
	 * This method is used to return list of books.
	 * 
	 * @return List<Book> This returns List of all Books.
	 */
	public List<Book> getAllBooks() {
		if (bookRepository.findAll().isEmpty()) {
			System.out.println("In get all books");
			throw new BookNotFoundException("BOOK LIST IS EMPTY");
		}
		return bookRepository.findAll();
	}

	/**
	 * This method is used to return book of particular Id
	 * 
	 * @param id This is the first parameter to getBookById method
	 * @return Book This returns a Book of that particular Id.
	 */
	public Book getBookById(int id) {
		return bookRepository.findById(id).orElseThrow(() -> new BookNotFoundException("BOOK NOT FOUND WITH ID " + id));
	}

	/**
	 * This method is used to add a new book
	 * 
	 * @param Book This is the first parameter to createBook method
	 */
	public Book createBook(Book insertBook) {
		Book book = new Book();
		book.setAuthor(insertBook.getAuthor());
		book.setBookName(insertBook.getBookName());
		book.setDescription(insertBook.getDescription());
		book.setGenre(insertBook.getGenre());
		return bookRepository.save(book);
	}

	/**
	 * This method is used to delete a particular book
	 * 
	 * @param id This is the first parameter to deleteBook method to delete
	 *           particular id
	 */
	public void deleteBook(int id) {
		Book book = bookRepository.findById(id)
				.orElseThrow(() -> new BookNotFoundException("BOOK NOT FOUND WITH ID " + id));
		bookRepository.delete(book);
	}

	/**
	 * This method is used to Update a book
	 * 
	 * @param id   is the first parameter to specify the book Id to update
	 * @param user This is the second parameter to updateBook method
	 */
	public void updateBook(int id, Book book) {
		Book book1 = bookRepository.findById(id)
				.orElseThrow(() -> new BookNotFoundException("BOOK NOT FOUND TO UPDATE WITH ID " + id));
		book1.setGenre(book.getGenre());
		book1.setAuthor(book.getAuthor());
		book1.setBookName(book.getBookName());
		book1.setDescription(book.getDescription());
		bookRepository.save(book1);
	}

}
