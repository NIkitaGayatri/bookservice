package org.com.pacttest;

import org.com.BooksServiceRestApplication;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.boot.SpringApplication;
import org.springframework.web.context.ConfigurableWebApplicationContext;

import au.com.dius.pact.provider.junit.PactRunner;
import au.com.dius.pact.provider.junit.Provider;
import au.com.dius.pact.provider.junit.loader.PactFolder;
import au.com.dius.pact.provider.junit.target.HttpTarget;
import au.com.dius.pact.provider.junit.target.Target;
import au.com.dius.pact.provider.junit.target.TestTarget;
@RunWith(PactRunner.class)
@Provider("book-service")
@PactFolder("/Users/Nikita_Gayatri/Test/Pact_Contracts")
public class PactProviderTest {
	@TestTarget
	public final Target target=new HttpTarget( "http", "10.71.12.198",8081,"/");
	public static ConfigurableWebApplicationContext application;

	@BeforeClass
	public static void start() {
		application = (ConfigurableWebApplicationContext) SpringApplication.run(BooksServiceRestApplication.class);
	}
}
