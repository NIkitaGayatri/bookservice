package org.com.restcontrollertest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.com.entities.Book;
import org.com.restcontrollers.BookRestController;
import org.com.services.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

 class BookRestControllerTest {
	@InjectMocks
	BookRestController bookController;
	@Mock
	BookService bookService;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetAllBooks() {
		List<Book> books = new ArrayList<>();
		books.add(new Book(1, "Wings of Fire", "Biography", "Tiwari Arun",
				"The book is full of insights, personal moments and life experiences of Dr. Kalam."));
		when(bookService.getAllBooks()).thenReturn(books);
		ResponseEntity<List<Book>> response = bookController.getAllBooks();
		assertThat(response.getStatusCodeValue()).isEqualTo(200);
		assertThat(response.getBody().size()).isEqualTo(books.size());
		assertEquals(response.getBody().get(0).getAuthor(), books.get(0).getAuthor());
		assertNotEquals(201, response.getStatusCodeValue());
	}

	@Test
	public void testGetBookById() {
		Book book = new Book(1, "Wings of Fire", "Biography", "Tiwari Arun",
				"The book is full of insights, personal moments and life experiences of Dr. Kalam.");
		when(bookService.getBookById(Mockito.anyInt())).thenReturn(book);
		ResponseEntity<Book> response = bookController.getBookById(1);
		assertThat(response.getStatusCodeValue()).isEqualTo(200);
	}

	@Test
	public void testAddBook() {
		Book book = new Book(1, "Wings of Fire", "Biography", "Tiwari Arun",
				"The book is full of insights, personal moments and life experiences of Dr. Kalam.");
		bookService.createBook(Mockito.any());
		ResponseEntity<Book> response = bookController.addBook(book);
		assertThat(response.getStatusCodeValue()).isEqualTo(201);
	}

	@Test
	public void testDeleteBook() {
		bookService.deleteBook(Mockito.anyInt());
		@SuppressWarnings("unchecked")
		ResponseEntity<Book> response = (ResponseEntity<Book>) bookController.deleteBook(1);
		assertThat(response.getStatusCodeValue()).isEqualTo(204);
	}

	@Test
	public void testUpdateBook() {
		Book book = new Book(1, "Wings of Fire", "Biography", "Tiwari Arun",
				"The book is full of insights, personal moments and life experiences of Dr. Kalam.");
		bookService.updateBook(Mockito.anyInt(), Mockito.any());
		ResponseEntity<Book> response = (ResponseEntity<Book>) bookController.updateBook(1, book);
		assertThat(response.getStatusCodeValue()).isEqualTo(201);
	}

}
