package org.com.servicetest;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.com.entities.Book;
import org.com.exception.BookNotFoundException;
import org.com.repository.BookRepository;
import org.com.services.BookService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.junit.jupiter.api.Assertions.*;
import org.mockito.MockitoAnnotations;


public class BookServiceTest {
	@InjectMocks
	BookService bookService;
	@Mock
	BookRepository bookRepository;
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void getAllBooksTest() {
		List<Book>bookList=new ArrayList<>();
		bookList.add(new Book(11,"J.k.rowling","Harrypoter","fantasy","Book contains information about harrypoter magic tricks"));
		bookList.add(new Book(21,"ramanujan","maths","logical","Book contains solutions for various math problems"));
		when(bookService.getAllBooks()).thenReturn(bookList);
	}
	@Test
	public void getBookIdTest() throws BookNotFoundException {
		Book book=new Book(1,"J.k.rowling","Harrypoter","fantasy","Book contains information about harrypoter magic tricks"); 
		Optional<Book> optional=Optional.of(book);
		when(bookRepository.findById(Mockito.anyInt())).thenReturn(optional);
		bookService.getBookById(1);
	}
	@Test
	public void createBookTest() {
		Book book=new Book(1,"J.k.rowling","Harrypoter","fantasy","Book contains information about harrypoter magic tricks"); 
		when(bookRepository.save(Mockito.any())).thenReturn(book);
		bookService.createBook(Mockito.any());
	}
	
	@Test
	public void deleteBookTest() throws BookNotFoundException {
		Book book=new Book(1,"J.k.rowling","Harrypoter","fantasy","Book contains information about harrypoter magic tricks");
	    Optional<Book> optional=Optional.of(book);
		when(bookRepository.findById(Mockito.anyInt())).thenReturn(optional);
		bookService.deleteBook(Mockito.anyInt());
	}
	@Test
	public void updateBookTest() throws BookNotFoundException {
		Book book=new Book(1,"J.k.rowling","Harrypoter","fantasy","Book contains information about harrypoter magic tricks");
	    Optional<Book> optional=Optional.of(book);
		when(bookRepository.findById(Mockito.anyInt())).thenReturn(optional);	
		bookService.updateBook(1,book);
	}
	@Test
    void exceptionTestForDelete() {
        Book book = null;
        Optional<Book> optional= Optional.ofNullable(book) ;
        when(bookRepository.findById(Mockito.anyInt())).thenReturn(optional);
        BookNotFoundException thrown = assertThrows(BookNotFoundException.class, () -> bookService.deleteBook(Mockito.anyInt()), "BOOK NOT FOUND WITH ID ");
        assertTrue(thrown.getMessage().contains("BOOK NOT FOUND WITH ID "));
    }
	@Test
    void exceptionTestBookById() {
        Book book = null;
        Optional<Book> optional= Optional.ofNullable(book) ;
        when(bookRepository.findById(Mockito.anyInt())).thenReturn(optional);
        BookNotFoundException thrown = assertThrows(BookNotFoundException.class, () -> bookService.getBookById(Mockito.anyInt()), "BOOK NOT FOUND WITH ID ");
        assertTrue(thrown.getMessage().contains("BOOK NOT FOUND WITH ID "));
    }
}
