package org.com.repsitorytest;

import static org.assertj.core.api.Assertions.assertThat;
import java.util.Optional;

import org.com.entities.Book;
import org.com.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
 class BookRepositoryTest {
	@Autowired
	BookRepository bookRepository;

//	@Test
//	public void testSaveBook() {
//		Book book = new Book();
//		book.setId(1);
//		book.setBookName("Harry Poter");
//		book.setAuthor("J.K.Rowling");
//		book.setGenre("fantasy");
//		book.setDescription("Book contains magic tricks");
//		bookRepository.save(book);
//		assertThat(book.getId()).isNotNull();
//	}
//
//	@Test
//	public void testFindAll() {
//		Book book = new Book(1, "J.k.rowling", "Harrypoter", "fantasy","Book contains magic tricks");
//		bookRepository.save(book);
//		Optional<Book> book2 = bookRepository.findById(1);
//		assertThat(book2).isNotEmpty();
//	}
}
